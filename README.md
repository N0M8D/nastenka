# Nástěnka

A web dashboard for the Czech Pirate Party, merging all of its systems'
notifications and actions into a single highly customizeable dashboard.
<!---
část sepsaná Honzou H
--> 
## Functional & Design requirements
- "mandatory" content box, set on admin level, that  will be always visible in all layouts as first on page (for party-wide anouncements, mandatory notifications and calls to action)
 - user-centric design
 - flexible layout (ability to change layout, ordering, filers, type etc. of main content boxes)
 - preconfigured layout (newbie, PKS, PMS, custom user-saved layout)
 - easy transition from dashboarb content boxes into source system in order to work with the "item" (whatever that is in the specific context)
 - both web and mobile optimized
 - integrated with PirateID
 - ability to restrict acces to some elements (or part of their content) based on membership in centrally managed 
 
### Content boxes
 - Mandatory "Notifications and anouncements" box used for notifying members for proceedings, vote starts and durations etc.
 - Simple Forum box for notifications from pirate forum (new messages, mentions, activity in watched subforum/thread)
 - Helios box with all votes, where active user is/was eligible to vote (with emphasizing those still open)
 - Zulip notification box
 - Mastodon notification box
 - Element notification box (if it makes it into supported system)
 - Mrak box (favorites folder)
 - Redmine box (my issues, issues reported by me, activity on relevant issues)
 - Important links box (pirtube, mrak, slido, etc. links provided by admins on CF/KF/MF level) with expiration (so they do not clutter gui after ceasing to be relevant)
 - URL shortener box (pi2.cz)
 - Social media boxes for official accounts (preset party & configured own for all main services)
 - Person search box (lide.pirati.cz)
 - calendar box (with different calendars that can be filtered)
 - piroplácení box (with different behavior for regular party member and payment administrator)

### Nice to have features
 - web-optimized version integrated into dedicated "pirate" app
 - cross-system search box
 - systems status box
 - Newsletters box (various newsletters with same configuration from user profile and ability to change the configuration from dashboard)

## Use case scenarios 
*All must work on any screen size unless stated otherwise*
### Party member
 - Configuring (customizing) dashboard
 - Using content box to access voting
 - Using content box to join discussion
 - Consume content (video, newsletter)
 ### RP/PKS/PMS
*Same as party member plus*
 - creating formal notification for corresponding level of proceeding (proceeding start, vote start and end, etc.)
 - creating other notification for corresponding level of party members (such as sharing important document link, PiTube video, jitsi link etc.)
 - manage corresponding level of calendar
 - manage rights and privileges in order to delegate any of the above to specific members of groups of members (namely helios voting messages)
 - creating payment order
 - managing payment order made by others
 ### Proceedings administrators
 - create notification about forum topic (member initiative, start of proceeding, start of vote, results)
 - share links related to above
 - manage calendar related to above
 ### KK/RK
 - ?? TBD

<!---
konec části sepsané Honzou H
--> 
## Planned implementations

### Internal (but publicly available) systems

- [Forum](https://wiki.pirati.cz/to/technicke-systemy/forum) (phpBB):
	- Followed threads with new posts
	- Recently active threads from MF, KF and CF
	- PMs
	- Mentions and replies
	- New "Global Announcement" threads
    - New threads in forum "Oznámení"
    - New CF threads
- [Helios](https://wiki.pirati.cz/to/technicke-systemy/helios):
	- New elections
	- Results of elections
- [Piroplácení](https://wiki.pirati.cz/to/technicke-systemy/piroplaceni) ([live version](https://piroplaceni.pirati.cz/)):
	- New requests to approve funding
	- New requests to pay out funding
- [Redmine](https://wiki.pirati.cz/to/technicke-systemy/redmine):
	- New and updated issues
	- New and updated tasks
- [Mrak](https://wiki.pirati.cz/to/technicke-systemy/mrak):
    - TODO
- [Zulip](https://wiki.pirati.cz/to/technicke-systemy/zulip):
	- New private messages
	- New mentions

### Public systems, social media

- [Mastodon](https://wiki.pirati.cz/to/technicke-systemy/mastodon) (ActivityPub):
	- Aggregated information on favorites, boosts and followers
	- Aggregated replies
	- New mentions
	- New private messages
- [PeerTube](https://wiki.pirati.cz/to/technicke-systemy/peertube) (ActivityPub):
    - New videos
    - Aggregated new likes and dislikes
    - Aggregated new comments
- [Newsletter](https://wiki.pirati.cz/po/newsletter):
    - Global newsletter
    - Newsletter from MF / KF
- [Press releases](https://github.com/pirati-web/pirati.cz):
    - New releases (possibly limited to certain authors)
- [Twitter](https://wiki.pirati.cz/mo/twitter):
	- Aggregated information on likes, retweets and followers
	- Aggreated replies
	- New mentions
	- New private messages
- [Facebook](https://wiki.pirati.cz/mo/facebook):
	- Aggregated information on likes and followers
	- Aggregated comments
	- New private messages
- [YouTube](https://wiki.pirati.cz/mo/youtube):
	- Aggregated information on likes and subscribers
	- Aggregated comments
