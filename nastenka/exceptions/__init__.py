"""Exceptions."""

from __future__ import annotations

import http.client

__all__ = [
	"WebException",
	"WebRateLimitExceeded"
]
__version__ = "0.0.1"


class WebException(Exception):
	"""The base class for all web exceptions."""

	code = http.client.INTERNAL_SERVER_ERROR
	"""The HTTP error code of an exception. By default, this will be ``500``."""

	details = None
	"""The details about an exception. :data:`None` by default."""

	def __init__(
		self: WebException,
		details: object = details,
		**kwargs
	) -> None:
		"""Sets the :attr:`details <.WebException.details>` class variable to the
		given value. If this method isn't used, it remains :data:`None`.

		:param details: The exception's details.
		"""

		self.details = details

		super().__init__(**kwargs)

	def __str__(self: WebException) -> str:
		"""Turns this instance into a human-readable string.

		:returns: The string.
		"""

		details = (
			self.details
			if self.details is not None
			else "no details"
		)

		return f"{self.__class__.__name__}, code {self.code} - {details}"

	def __repr__(self: WebException) -> str:
		"""Turns this instance into a typical ``__repr__`` string.

		:returns: The string.
		"""

		return (
			f"<{self.__class__.__name__}(code={self.code}, details={self.details})>"
		)


class WebRateLimitExceeded(APIException):
	"""Exception class for when a user has exceeded their rate limit for a
	specific view.

	.. seealso::
		:class:`nastenka.limiter.Limiter`
	"""

	code = http.client.TOO_MANY_REQUESTS
