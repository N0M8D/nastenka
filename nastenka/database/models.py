import datetime

import sqlalchemy

from . import Base
from .utils import UUID, get_uuid

__all__ = ["Model"]


class Model(Base):
	__tablename__ = "models"

	id = sqlalchemy.Column(
		UUID,
		primary_key=True,
		default=get_uuid
	)
